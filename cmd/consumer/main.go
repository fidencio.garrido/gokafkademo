package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

var delay = time.Millisecond * 2

// this method is just to simulate an async operation that would typically take a few milliseconds, like storing in
// a database
func storeInDB(message string) {
	time.Sleep(delay)
}

// ToDo: Delay shutdown until is safe
func main() {
	pollTimeoutFlag := flag.Duration("t", 1*time.Second, "Kafka poll timeout")
	printStats := flag.Bool("s", false, "Print topic stats")
	flag.Parse()

	pollTimeout := int(*pollTimeoutFlag / time.Millisecond)

	fmt.Printf("Kafka poll timeout: %s = %d ms\n", *pollTimeoutFlag, pollTimeout)

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":      "localhost",
		"group.id":               "myGroup",
		"auto.offset.reset":      "earliest",
		"statistics.interval.ms": 15000,
		"session.timeout.ms":     6000,
	})

	if err != nil {
		panic(err)
	}

	c.SubscribeTopics([]string{"myTopic", "^aRegex.*[Tt]opic"}, nil)

	start := time.Now()
	keepLoopRunning := true

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	for keepLoopRunning { // our infinite loop
		select {
		case sig := <-sigchan: // k8s or someone else is calling us to quite, try it, press ctrl+c
			fmt.Printf("The operating system is asking us to quit: %v\n", sig)
			// unsubscribe and stop consuming to avoid leaving transactions unfinished
			c.Unsubscribe()
			keepLoopRunning = false
			// wait for a few seconds before disconnecting, no messages should come in at this time
			time.Sleep(5 * time.Second)
		default:
			ev := c.Poll(pollTimeout)
			if ev == nil { // no messages available
				continue // restart infinite loop to poll again
			}
			switch e := ev.(type) {
			case *kafka.Message:
				storeInDB(string(e.Value))
				fmt.Printf("[%s] Message on %s: %s\n", time.Since(start), e.TopicPartition, string(e.Value))
			case kafka.Error:
				// The client will automatically try to recover from all errors.
				fmt.Printf("Consumer error: %v (%v)\n", err, e)
			case kafka.AssignedPartitions:
				fmt.Printf("Assigned partition %s\n", e.String())
			case kafka.RevokedPartitions:
				fmt.Printf("Revoked partition: %s\n", e.String())
			case kafka.PartitionEOF:
				fmt.Printf("reached out of partition: %d\n", e.Partition)
			case kafka.OffsetsCommitted:
				fmt.Printf("offsets commited: %v\n", e.Offsets)
			case *kafka.Stats:
				if *printStats {
					var stats map[string]interface{}
					err := json.Unmarshal([]byte(e.String()), &stats)
					if err == nil {
						for k, v := range stats {
							fmt.Printf("%s = %v\n", k, v)
						}
					} else {
						fmt.Printf("cannot read consumer stats: %s\n", err)
					}
				}
			default:
				fmt.Printf("Ignoring unexpected message %v\n", e)
			}
		}

	}

	c.Close()
	log.Println("Bye!")
}
