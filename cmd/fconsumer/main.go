package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

var delay = time.Millisecond * 2

const workers = 10

// this method is just to simulate an async operation that would typically take a few milliseconds, like storing in
// a database
func storeInDB(message string) {
	time.Sleep(delay)
}

func main() {
	pollTimeoutFlag := flag.Duration("t", 1*time.Second, "Kafka poll timeout")
	flag.Parse()

	pollTimeout := int(*pollTimeoutFlag / time.Millisecond)

	fmt.Printf("Kafka poll timeout: %s = %d ms\n", *pollTimeoutFlag, pollTimeout)

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost",
		"group.id":          "myGroup",
		"auto.offset.reset": "earliest",
	})

	if err != nil {
		panic(err)
	}

	c.SubscribeTopics([]string{"myTopic", "^aRegex.*[Tt]opic"}, nil)

	messagesChannel := make(chan *kafka.Message, 1)
	start := time.Now()

	for i := 0; i < workers; i++ {
		go func() {
			for msg := range messagesChannel {
				storeInDB(string(msg.Value))
				fmt.Printf("[%s] Message on %s: %s\n", time.Since(start), msg.TopicPartition, string(msg.Value))
			}
		}()
	}

	keepLoopRunning := true

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	for keepLoopRunning { // our infinite loop
		select {
		case sig := <-sigchan: // k8s or someone else is calling us to quite, try it, press ctrl+c
			fmt.Printf("The operating system is asking us to quit: %v\n", sig)
			keepLoopRunning = false
		default:
			ev := c.Poll(pollTimeout)
			if ev == nil { // no messages available
				continue // restart infinite loop to poll again
			}
			switch e := ev.(type) {
			case *kafka.Message:
				messagesChannel <- e
			case kafka.Error:
				// The client will automatically try to recover from all errors.
				fmt.Printf("Consumer error: %v (%v)\n", err, e)
			default:
				fmt.Printf("Ignoring unexpected message %v\n", e)
			}
		}

	}

	c.Close()
}
