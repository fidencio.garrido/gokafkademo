package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

var delay = time.Millisecond * 2

const workers = 10

// this method is just to simulate an async operation that would typically take a few milliseconds, like storing in
// a database
func storeInDB(message string) error {
	time.Sleep(delay)
	return nil
}

func main() {
	pollTimeoutFlag := flag.Duration("t", 1*time.Second, "Kafka poll timeout")
	flag.Parse()

	pollTimeout := int(*pollTimeoutFlag / time.Millisecond)

	fmt.Printf("Kafka poll timeout: %s = %d ms\n", *pollTimeoutFlag, pollTimeout)

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":  "localhost",
		"group.id":           "myGroup2",
		"auto.offset.reset":  "earliest",
		"enable.auto.commit": false,
	})

	if err != nil {
		panic(err)
	}

	// You can subscribe to multiple topics at once, but don't forget to check when you
	// receive what's the topic source for each message
	c.SubscribeTopics([]string{"myTopic", "^aRegex.*[Tt]opic"}, nil)

	messagesChannel := make(chan *kafka.Message, 1)
	start := time.Now()

	for i := 0; i < workers; i++ {
		go func() {
			for msg := range messagesChannel {
				err := storeInDB(string(msg.Value))
				if err == nil { // all good
					partition, err := c.CommitMessage(msg) // manually message commit
					if err == nil {
						// call on commit
						fmt.Printf("[%s] Message on %s: %s\n", time.Since(start), msg.TopicPartition, string(msg.Value))
						if len(partition) > 0 {
							fmt.Printf("Message was committed in partition: %d\n", partition[0].Partition)
						}
					} else { //weird case, what do we do if the DB stored but you can't commit the offset?
						// provide a solution appropiate for your case
					}

				} else { // db returned an error... what do we do?
					// manage the error somehow please or commit
				}
			}
		}()
	}

	keepLoopRunning := true

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	for keepLoopRunning { // our infinite loop
		select {
		case sig := <-sigchan: // k8s or someone else is calling us to quite, try it, press ctrl+c
			fmt.Printf("The operating system is asking us to quit: %v\n", sig)
			keepLoopRunning = false
		default:
			ev := c.Poll(pollTimeout)
			if ev == nil { // no messages available
				continue // restart infinite loop to poll again
			}
			switch e := ev.(type) {
			case *kafka.Message:
				// call on received
				messagesChannel <- e
			case kafka.Error:
				// call on error
				// The client will automatically try to recover from all errors.
				fmt.Printf("Consumer error: %v (%v)\n", err, e)
			default:
				fmt.Printf("Ignoring unexpected message %v\n", e)
			}
		}

	}

	c.Close()
}
