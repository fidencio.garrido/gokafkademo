# Kafka + Go demo

Requirements:
- Docker and Docker compose
- Know your IP :) (you can use ifconfig)
- Xcode
- librdkafka and pkg-config. OSX: ```brew install librdkafka pkg-config```

## Folders content

Consumers:
- Basic: ```cmd/consumer```
- Fast consumer with autocommit: ```cmd/fconsumer```
- Fast consumer with manual commit and basic error handling: ```cmd/fmanconsumer```

Producer:
- Basic: ```cmd/producer```

## Running the demo

### Provisioning your own local kafka for testing

Simply:
- Edit the ```docker-compose.yaml``` file and modify line 12 with your local IP
- Execute ```docker-compose up``` from that directory
- That's it!

## Consumers

### Benchmark

For the test, the demo simulates a service in which a message received is stored in
a database with a latency of 1ms. The "fast" version uses a simple worker pool
using channels and goroutines. The results here shall not be used to predict
your own performance. You should explore your system to determine if using
a workers pool fits your needs and, if so, you should test different configurations
to determine the right worker pool size. The only intention of the example is
to illustrate how Go provides facilities to manage the performance and throughput.

| Consumer | Time   |
|----------|--------|
| Basic    | 21.85s |
| Fast     | 6.66s  |

Considerations:
- Higher CPU and memory utilization, not bad, but for sure something to keep an eye on
- Commits might go out of sync since workers will finish in a different rate
- Pace the message handling so that you don't overload the downstream of the message
(for example the database) 