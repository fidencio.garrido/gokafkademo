module gitlab.com/fidencio.garrido/gokafkademo

go 1.13

require gopkg.in/confluentinc/confluent-kafka-go.v1 v1.1.0
